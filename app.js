'use strict';
var app = angular.module('ng1RNews', ['ui.router']);

app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: '/home.html',
            controller: 'MainCtrl'
        })
        .state('posts', {
            url: '/posts/{id}',
            templateUrl: '/posts.html',
            controller: 'PostsCtrl'
        });
    $urlRouterProvider.otherwise('home');
}]);

app.factory('posts', [function() {
    var o = {
        posts: [{
                title: 'Just Angular',
                link: 'https://angular.io/',
                upvotes: 169,
                comments: [{
                        body: 'the future of web development',
                        author: 'joel',
                        upvotes: 26
                    },
                    {
                        body: 'it\'s HTML6 today',
                        author: 'mary',
                        upvotes: 39
                    }
                ]
            },
            {
                title: 'AngularJS',
                link: 'https://angularjs.org/',
                upvotes: 113,
                comments: [{
                        body: 'is an awesome spa framework',
                        author: 'max',
                        upvotes: 10
                    },
                    {
                        body: 'i love it!!!',
                        author: 'jane',
                        upvotes: 17
                    }
                ]
            },
            {
                title: 'Angular CLI',
                link: 'https://cli.angular.io',
                upvotes: 94,
                comments: [{
                        body: 'create fast, performant ng apps',
                        author: 'anthony',
                        upvotes: 29
                    },
                    {
                        body: 'win time and focus on your app logic',
                        author: 'barbara',
                        upvotes: 43
                    }

                ]
            }
        ]
    };
    return o;
}]);

app.controller('MainCtrl', ['$scope', 'posts', function($scope, posts) {

    $scope.posts = posts.posts;

    $scope.addPost = function() {
        if (!$scope.title || $scope.title === '') {
            return;
        }
        $scope.posts.push({
            title: $scope.title,
            link: $scope.link,
            upvotes: 0,
            comments: [{
                    author: 'joe',
                    body: 'cool post',
                    upvotes: 0
                },
                {
                    author: 'bob',
                    body: 'great idea but everything is wrong!',
                    upvotes: 0
                }
            ]
        });
        $scope.title = '';
    };

    $scope.incrementUpvotes = function(post) {
        post.upvotes += 1;
    };

}]);

app.controller('PostsCtrl', ['$scope', '$stateParams', 'posts', function($scope, $stateParams, posts) {

    $scope.post = posts.posts[$stateParams.id];

    $scope.addComment = function() {
        if ($scope.body === '') {
            return;
        }
        $scope.post.comments.push({
            body: $scope.body,
            author: 'user',
            upvotes: 0
        });
        $scope.body = '';
    };

    $scope.incrementUpvotes = function(post) {
        post.upvotes += 1;
    };
}]);
